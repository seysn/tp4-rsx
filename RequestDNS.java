import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;

class RequestDNS {
	public static void main(String[] args) throws Exception {
		RequestDNS rdns = new RequestDNS();

		if (args[0] == null) {
			System.out.println("cmd <hostname>");
			System.exit(1);
		}

		System.out.println(rdns.byteArrayToHex(rdns.createRequest(args[0])));
		System.out.println(rdns.sendUdp(rdns.createRequest(args[0]), "193.49.225.15"));
	}

	public byte[] createRequest(String addr) {
		ByteBuffer buf = ByteBuffer.allocate(addr.length() + 18);
		String[] s_addr = addr.split("\\.");

		buf.put(new byte[] {
				(byte)0x47, (byte)0x98, (byte)0x01, (byte)0x00,
				(byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00,
				(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00
			});

		for (String s : s_addr) {
			buf.put((byte) s.length());
			for (char c : s.toCharArray()) {
				buf.put((byte) c);
			}
		}

		buf.put(new byte[] {
				(byte)0x00, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x01
			});

		return buf.array();
	}

	public String sendUdp(byte[] sendData, String dnsServer) throws Exception {
		DatagramSocket clientSocket = new DatagramSocket();
		InetAddress IPAddress = InetAddress.getByName(dnsServer);
		byte[] receiveData = new byte[1024];

		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 53);
		System.out.println("[info] SENDING DATA TO " + dnsServer);
		clientSocket.send(sendPacket);
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		System.out.println("[info] RECEIVING DATA");
		clientSocket.receive(receivePacket);
		System.out.println("[info] DATA RECEIVED");
		//String modifiedSentence = new String(receivePacket.getData());

		clientSocket.close();

		return byteArrayToHex(receivePacket.getData());
	}

	public static String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for(byte b: a)
			sb.append(String.format("%02x ", b));
		return sb.toString();
	}
}
