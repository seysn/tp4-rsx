#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Usage: SendUDP <msg>!\n");
		exit(-1);
	}

	int sock = socket(AF_INET, SOCK_DGRAM, 0);

	if (sock == -1) {
		perror("socket");
		exit(-1);
	}

	struct sockaddr_in sin = { 0 };
	sin.sin_family = AF_INET;
	sin.sin_port = htons(7654);
	sin.sin_addr.s_addr = inet_addr("127.0.0.1");

	if (sin.sin_addr.s_addr == INADDR_NONE) {
		perror("bad address");
		exit(-1);
	}

	sendto(sock, argv[1], strlen(argv[1]), 0,
		   (struct sockaddr *) &sin, sizeof sin);
	
	return 0;
}
